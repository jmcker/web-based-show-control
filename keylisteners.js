function keyListeners(cueEdit, titleEdit, newCue, editCue) {
    document.onkeydown = function(event) {
        event = event || window.event;

        if (!(cueEdit || titleEdit || newCue || editCue)) {
            // Listen for t to open edit title.
            if (event.keyCode == 84) {
                editTitle();
                event.preventDefault();
            }

            // Listen for up arrow key
            if (event.keyCode == 38) {
                if (currentCue > 1) {
                    currentCue--;
                }
                select(currentCue);
            }

            // Listen for down arrow key
            if (event.keyCode == 40) {
                if (currentCue < cueListLength) {
                    currentCue++;
                }
                select(currentCue);
            }
            
            if (event.keyCode == 32) {
                go(currentCue);
            }
            
            // Listen for escape.
            if (event.keyCode == 27) {
                closeAll();
                hideNewCueMenu();
                hideEditCueMenu();
                stopAll();
            }
        }

        // Listen for escape but don't kill playing sounds.
        if (event.keyCode == 27) {
            closeAll();
            hideNewCueMenu();
            hideEditCueMenu();
        }
        // Listen for control enter when a cue list field is being edited.
        if (cueEdit) {
            if ((event.keyCode == 10 || event.keyCode == 13)) {
                closeAll();
            }
        }
        // Listen for control enter when title edit is selected.
        if (titleEdit) {
            if ((event.keyCode == 10 || event.keyCode == 13)) {
                closeTitle(true);
            }
        }
        // Listen for enter in the new cue window.
        if (newCue) {
            if ((event.keyCode == 10 || event.keyCode == 13)) {
                createCue();
            }
        }
        // Listen for ctrl-enter in the edit cue window.
        if (editCue) {
            if ((event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey) {
                saveEditedCue();
            }
        }
    };
}