var context;
var currentlyPlaying = {};
var previewing = null;
var globalPanControl = document.getElementById("global_pan_control");
var globalGainControl = document.getElementById("global_gain_control");
var globalPanDisplay = document.getElementById("global_pan_display");
var globalGainDisplay = document.getElementById("global_gain_display");

try {
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    context = new AudioContext();
} catch (e) {
    alert('Web Audio API is not supported in this browser.');
}

var globalPanNode = context.createStereoPanner();
var globalGainNode = context.createGain();
globalPanNode.connect(globalGainNode);
globalGainNode.connect(context.destination);
setGlobalPan(globalPanControl.value);
setGlobalGain(globalGainControl.value);
globalGainNode.gain.value = dBToGain(globalGainControl.value);
globalPanControl.oninput = function() { setGlobalPan(globalPanControl.value); };
globalGainControl.oninput = function() { setGlobalGain(globalGainControl.value); };
globalPanControl.ondblclick = function() { setGlobalPan(0); };
globalGainControl.ondblclick = function() { setGlobalGain(0); };

function setGlobalGain(dB) {
    globalGainControl.value = dB;
    globalGainDisplay.innerHTML = dB + "dB";
    globalGainNode.gain.value = dBToGain(dB);
}

function setGlobalPan(pan) {
    globalPanControl.value = pan;
    globalPanDisplay.innerHTML = pan;
    globalPanNode.pan.value = pan / 50;
}

function dBToGain(dB) {
    if (dB == -60)
        return 0;
    return Math.pow(10, dB / 20.0);
}

function gainTodB(gain) {
    return Math.log(gain) / Math.LN10;
}

class Playback {

    constructor(context, cueNum) {
        this.context = context;
        this.cueNum = cueNum;
        this.globalPanNode = globalPanNode;
        this.globalGainNode = globalGainNode;
    }

    init() {
        this.source1 = this.context.createBufferSource();
        this.panNode = this.context.createStereoPanner();
        this.gainNode = this.context.createGain();

        this.source1.connect(this.panNode);
        this.panNode.connect(this.gainNode);
        this.gainNode.connect(this.globalPanNode);

        this.panNode.pan.value = this.pan;
        this.gainNode.gain.value = this.gain;
        if (this.fadeInTime > 0) {
            this.gainNode.gain.value = 0.01;
        }
    }

    syncParams() {
        this.pan = getPan(this.cueNum) / 50;
        this.vol = getVol(this.cueNum);
        this.gain = dBToGain(getVol(this.cueNum));
        this.startPos = getStartPosInSecs(this.cueNum);
        this.stopPos = getStopPosInSecs(this.cueNum);
        this.fileDuration = getFileDurInSecs(this.cueNum);
        this.cueDuration = this.stopPos - this.startPos;
        this.fadeInTime = getFadeIn(this.cueNum);
        this.fadeOutTime = getFadeOut(this.cueNum);
        this.loops = getLoops(this.cueNum);
    }

    play(contextStart, contextStop) {
        if (!hasAudioFile(this.cueNum)) {
            alert("Could not play cue. No file loaded in Cue #" + cueNum + ".");
            return;
        }
        
        this.syncParams();
        this.init();
        
        this.contextStart = contextStart || this.context.currentTime;
        this.contextStop = contextStop || this.context.currentTime + this.cueDuration;
        
        var self = this;
        this.obtainBytes(getAudioFile(this.cueNum), function(bytesAsArrayBuffer) {
            self.decodeBytesAndPlay(bytesAsArrayBuffer);
        }); // Plays the cue

        console.log("current time: " + this.context.currentTime);
        console.log("start pos: " + this.startPos);
        console.log("stop pos: " + this.stopPos);
        console.log("stop: " + this.contextStop);
        console.log("loops: " + this.loops);

        var loopCount = 0;
        this.intervalId = setInterval(function() {
            self.updateTimeDisplays(self.contextStart + loopCount * self.cueDuration);
            if (self.context.currentTime >= self.contextStop + loopCount * self.cueDuration) {
                if (loopCount === self.loops - 1) {
                    self.stop(0);
                    self = null;
                } else {
                    resetProgressBar(self.cueNum);
                    loopCount++;
                }
            }
        }, 100);
    }

    stop(time) {
        console.log("Stopped Cue #" + this.cueNum + ".");
        
        this.source1.stop(time);
        clearInterval(this.intervalId);
        setElapsed(this.cueNum, null);
        setRemaining(this.cueNum, null);
        resetProgressBar(this.cueNum);
        delete currentlyPlaying[this.cueNum];
        this.source1 = null;
        this.gainNode = null;
        this.panNode = null;
        console.log("Removed Cue #" + this.cueNum + " from the currently playing list.");
        console.dir(currentlyPlaying);
        checkButtonLock();
    }
    
    fadeIn(time) {
        console.log("fade in context time: " + time);
        this.gainNode.gain.exponentialRampToValueAtTime(this.gain, time);
    }
    
    fadeOut(time) {
        console.log("fade out context time: " + time);
        this.gainNode.gain.setTargetAtTime(0, time, 1); // Apprx exponential fade
    }
    
    updateTimeDisplays(contextStart) {
        var now = this.context.currentTime;
        var elapsed = (now - contextStart).toFixed(2);
        var remaining = (this.cueDuration - elapsed).toFixed(2);
        var percentDone = parseInt(elapsed / this.cueDuration * 100);
        
        console.log("elapsed: " + elapsed);
        console.log("remaining: " + remaining);
        console.log("percent done: " + percentDone);
        
        setElapsed(this.cueNum, elapsed);
        setRemaining(this.cueNum, remaining);
        updateProgressBar(this.cueNum, percentDone, remaining);
    }

    obtainBytes(selectedFile, callback) {
        var reader = new FileReader();
        reader.onload = function(ev) {
            var bytesAsArrayBuffer = reader.result;
            callback(bytesAsArrayBuffer);
        };
        reader.readAsArrayBuffer(selectedFile);
    }

    decodeBytesAndPlay(bytesAsArrayBuffer) {
        var self = this;
        this.context.decodeAudioData(bytesAsArrayBuffer, function(decodedSamplesAsAudioBuffer) {
            self.source1.buffer = decodedSamplesAsAudioBuffer;
            
            self.source1.loop = true;
            self.source1.loopStart = self.startPos;
            self.source1.loopEnd = self.stopPos;
            if (self.fadeInTime > 0)
                self.fadeIn(self.contextStart + self.fadeInTime);
            if (self.fadeOutTime > 0)
                self.fadeOut(self.context.currentTime + self.cueDuration * self.loops - self.fadeOutTime);
            
            self.source1.start(self.contextStart, self.startPos);
            self.source1.stop(self.context.currentTime + self.cueDuration * self.loops);
        });
    }
}


class Wait {
    
    constructor(context, cueNum) {
        this.context = context;
        this.cueNum = cueNum;
        this.action = "SP";
    }
    
    init() {
        this.target = getTargetId(this.cueNum);
        this.duration = getCueDurInSecs(this.cueNum);
    }
    
    play(contextStart, contextStop) {
        this.init();
        
        contextStart = contextStart || this.context.currentTime;
        contextStop = contextStop || this.context.currentTime + this.duration;
        
        console.log("start: " + contextStart);
        console.log("stop: " + contextStop);
        
        var self = this;
        this.intervalId = setInterval(function() {
            self.updateTimeDisplays(contextStart);
            if (context.currentTime >= contextStop) {
                self.stop(0);
                self = null;
            }
        }, 100);
    }
    
    stop(time) {
        console.log("Stopped Wait Cue #" + this.cueNum + ".");
        
        clearInterval(this.intervalId);
        setElapsed(this.cueNum, null);
        setRemaining(this.cueNum, null);
        resetProgressBar(this.cueNum);
        delete currentlyPlaying[this.cueNum];
        console.log("Removed Cue #" + this.cueNum + " from the currently playing list.");
        console.dir(currentlyPlaying);
        checkButtonLock();
        advance(this.target, this.action);
    }
    
    updateTimeDisplays(contextStart) {
        var now = this.context.currentTime;
        var elapsed = (now - contextStart).toFixed(2);
        var remaining = (this.duration - elapsed).toFixed(2);
        var percentDone = parseInt(elapsed / this.duration * 100);
        
        console.log("elapsed: " + elapsed);
        console.log("remaining: " + remaining);
        console.log("percent done: " + percentDone);
        
        setElapsed(this.cueNum, elapsed);
        setRemaining(this.cueNum, remaining);
        updateProgressBar(this.cueNum, percentDone, remaining);
    }
}


class Preview {

    constructor(context) {
        this.context = context;
        this.globalPanNode = globalPanNode;
        this.globalGainNode = globalGainNode;
    }

    init() {
        this.source1 = this.context.createBufferSource();
        this.panNode = this.context.createStereoPanner();
        this.gainNode = this.context.createGain();

        this.source1.connect(this.panNode);
        this.panNode.connect(this.gainNode);
        this.gainNode.connect(this.globalPanNode);

        this.panNode.pan.value = this.pan;
        this.gainNode.gain.value = this.gain;
        if (this.fadeInTime > 0) {
            this.gainNode.gain.value = 0.01;
        }
    }

    syncParams() {
        var evol = document.getElementById("vol_in");
        var epan = document.getElementById("pan_in");
        var epitch = document.getElementById("pitch_in");
        var efile = document.getElementById("edit_file");
        var efilelength = document.getElementById("edit_file_length");
        var efadein = document.getElementById("edit_fade_in");
        var efadeout = document.getElementById("edit_fade_out");
        var eloops = document.getElementById("edit_loops");
        
        this.vol = parseInt(evol.value);
        this.gain = dBToGain(this.vol);
        this.pan = parseInt(epan.value);
        this.pitch = parseInt(epitch.value);
        this.file = efile.files[0] || allCueAudioFiles[currentCue];
        this.startPos = getEditStartPos();
        this.stopPos = getEditStopPos();
        this.fileDuration = timeToSec(efilelength.innerHTML);
        this.cueDuration = this.stopPos - this.startPos;
        this.fadeInTime = parseInt(efadein.value);
        this.fadeOutTime = parseInt(efadeout.value);
        this.loops = parseInt(eloops.value);
    }

    play(contextStart, contextStop) {
        
        this.syncParams();
        this.init();
        
        if (!this.file) {
            alert("Could not preview cue. No file loaded.");
            return;
        }
        
        var preview = document.getElementById("edit_preview");
        var self = this;
        
        setButtonLock(true);
        preview.innerHTML = "Stop Preview";
        preview.onclick = function() { self.stop(0); };
    
        
        this.contextStart = contextStart || this.context.currentTime;
        this.contextStop = contextStop || this.context.currentTime + this.cueDuration;
        
        previewing = this;
        this.obtainBytes(this.file, function(bytesAsArrayBuffer) {
            self.decodeBytesAndPlay(bytesAsArrayBuffer);
        }); // Plays the cue

        console.log("current time: " + this.context.currentTime);
        console.log("start pos: " + this.startPos);
        console.log("stop pos: " + this.stopPos);
        console.log("stop: " + this.contextStop);
        console.log("loops: " + this.loops);

        var loopCount = 0;
        this.intervalId = setInterval(function() {
            if (self.context.currentTime >= self.contextStop + loopCount * self.cueDuration) {
                if (loopCount === self.loops - 1) {
                    self.stop(0);
                    self = null;
                } else {
                    loopCount++;
                }
            }
        }, 100);
    }
    
    fade(time, length) {
        
    }

    stop(time) {
        this.source1.stop(time);
        clearInterval(this.intervalId);
        this.source1 = null;
        console.log("Stopped preview.");
        
        var preview = document.getElementById("edit_preview");
        preview.innerHTML = "Preview";
        preview.setAttribute('onclick', 'preview()');
        setButtonLock(false);
        
        previewing = null;
        console.dir(previewing);
    }
    
    fadeIn(time) {
        console.log("fade in context time: " + time);
        this.gainNode.gain.exponentialRampToValueAtTime(this.gain, time);
    }
    
    fadeOut(time) {
        console.log("fade out context time: " + time);
        this.gainNode.gain.setTargetAtTime(0, time, 1); // Apprx exponential fade
    }

    obtainBytes(selectedFile, callback) {
        var reader = new FileReader();
        reader.onload = function(ev) {
            var bytesAsArrayBuffer = reader.result;
            callback(bytesAsArrayBuffer);
        };
        reader.readAsArrayBuffer(selectedFile);
    }

    decodeBytesAndPlay(bytesAsArrayBuffer) {
        var self = this;
        this.context.decodeAudioData(bytesAsArrayBuffer, function(decodedSamplesAsAudioBuffer) {
            self.source1.buffer = decodedSamplesAsAudioBuffer;
            self.source1.loop = true;
            self.source1.loopStart = self.startPos;
            self.source1.loopEnd = self.stopPos;
            if (self.fadeInTime > 0)
                self.fadeIn(self.contextStart + self.fadeInTime);
            if (self.fadeOutTime > 0)
                self.fadeOut(self.context.currentTime + self.cueDuration * self.loops - self.fadeOutTime);
            
            self.source1.start(self.contextStart, self.startPos);
            self.source1.stop(self.context.currentTime + self.cueDuration * self.loops);
        });
    }
}

function syncDataFromSourceBuffer(cueNum, file) {
    cueNum = cueNum || currentCue;
    file = file || allCueAudioFiles[cueNum];
    
    if (!allCueAudioFiles[cueNum] && !file) {
        setEditFileLength(null);
        setFileDur(cueNum, null);
        setCueDur(cueNum, null);
        console.log("No file found for Cue #" + cueNum + ".");
        return;
    }
    
    var bufferSource = context.createBufferSource();
    var reader = new FileReader();
    reader.onload = function() {
        var audioData = reader.result;
        context.decodeAudioData(audioData, function(buffer) {
            bufferSource.buffer = buffer;
            var dur = (bufferSource.buffer.duration).toFixed(3);
            // Don't set actual cue duration in case user selects cancel
            setEditStartPos([0, 0, 0]);
            setEditStartPosMax(secToArray(dur));
            setEditStopPos(secToArray(dur));
            setEditStopPosMax(secToArray(dur));
            setEditFileLength(parseFloat(dur).toFixed(2));
            setEditButtonLock(false);
        });
    };
    reader.readAsArrayBuffer(file);
    
    return; // Tell GC that we're done
}

function go(cueNum) {
    cueNum = cueNum || currentCue;

    // Allows a different cue to target a memo cue
    // Kept the behavior so that it could be used to traverse a cue list with Act/Scene markers and not worry about accidentally triggering a cue
    if (getType(cueNum) === "memo") {
        select(cueNum);
        return;
    }
    if (getType(cueNum) === "wait") {
        var wait = new Wait(context, cueNum);
        wait.play(0);
        currentlyPlaying[cueNum] = wait;
        return;
    }
    if (!getAudioFile(cueNum)) {
        alert("No file found for Cue #" + cueNum + ".");
        return;
    }
    if (currentlyPlaying[cueNum]) {
        console.log("Cue #" + cueNum + " is already playing.");
        return;
    }

    var cue = new Playback(context, cueNum);
    cue.play(0);
    currentlyPlaying[cueNum] = cue;
    advance();
}

function preview(cueNum) {
    cueNum = cueNum || currentCue;
    
    var preview = new Preview(context);
    preview.play(0);
}

function stop(cueNum) {
    cueNum = cueNum || currentCue;

    if (currentlyPlaying[cueNum]) {
        currentlyPlaying[cueNum].stop(context.currentTime);
    }
    // Checked button lock already in cue.stop();
}

function advance(targetId, action) {
    action = action || getAction(currentCue);
    targetId = targetId || getTargetId(currentCue);

    // Target is next cue and next cue does not exist
    if (targetId > cueListLength || targetId === 0 && currentCue + 1 > cueListLength) {
        checkButtonLock();
        return;
    } else if (targetId === 0) {
    // Target is next cue--lock targetId to actual cue number
        targetId = currentCue + 1;
    }

    // If cue is not engaged, try cue after it
    if (getEnabled(targetId) === false) {
        advance(targetId + 1);
        return;
    }

    currentCue = targetId;
    select(currentCue);
    if (action === "SP") {
        go(targetId);
    }
}

function stopAll() {
    for (var key in currentlyPlaying) {
        currentlyPlaying[key].stop(context.currentTime);
    }
    // Checked button lock already in cue.stop()
}