var currentCue = 0;
var cueListLength = 0;
var allCueAudioFiles = {};

createCue("audio");
currentCue = 1;
select(currentCue);
keyListeners(false, false, false, false);


function show(id) {
    var element = document.getElementById(id);
    element.className = element.className.replace("hidden", "visible");
}

function hide(id) {
    var element = document.getElementById(id);
    element.className = element.className.replace("visible", "hidden");
}

function save(element) {
    var id = parseInt(element.id);
    var display = document.getElementById(id + 1);
    display.innerHTML = element.value;
}

function open(element) {
    var id = parseInt(element.id);
    element.className = element.className.replace("hidden", "open");
    hide(id + 1); // Hide display element to show edit box
}

function close(element) {
    var id = parseInt(element.id);
    element.className = element.className.replace("open", "hidden");
    show(id + 1); // Show display element in place of edit box
}

function saveAll() {
    var opened = document.getElementsByClassName("open");
    for (var i = 0; i < opened.length; i++) {
        save(opened[i]);
    }
}

function closeAll() {
    saveAll();
    closeTitle(false);
    var opened = document.getElementsByClassName("open");
    for (var i = 0; i < opened.length; i++) {
        close(opened[i]);
    }
    keyListeners(false, false, false, false);
}

function edit(id) {
    closeAll();
    var element = document.getElementById(id);
    open(element);
    element.select();
    keyListeners(true, false, false, false);
}

function editTitle() {
    show("edit_title");
    show("save_title");
    show("cancel_title");
    hide("display_title");

    var text = document.getElementById("edit_title");
    text.focus();

    keyListeners(false, true, false, false);
}

function closeTitle(saveChanges) {
    hide("edit_title");
    hide("save_title");
    hide("cancel_title");
    show("display_title");

    var text = document.getElementById("edit_title");
    var display = document.getElementById("display_title");
    if (saveChanges) {
        if (text.value.length === "") {
            display.innerHTML = "<empty>";
        } else {
            display.innerHTML = text.value;
        }
    } else {
        text.value = display.innerHTML;
    }

    keyListeners(false, false, false, false);
}

function editCue(cueNum) {
    cueNum = cueNum || currentCue;
    var ctype = getType(cueNum);
    
    // Stop preview if revert button is clicked
    if (typeof previewing !== "undefined" && previewing) {
        previewing.stop(0);
    }

    var etitle = document.getElementById("edit_menu_title");
    var enotes = document.getElementById("edit_notes");
    var edesc = document.getElementById("edit_desc");
    var evol = document.getElementById("vol_in");
    var epan = document.getElementById("pan_in");
    var epitch = document.getElementById("pitch_in");
    var eaction = document.getElementById("edit_action");
    var etarget = document.getElementById("edit_target");
    var efile = document.getElementById("edit_file");
    var efilename = document.getElementById("edit_filename");
    var efilelength = document.getElementById("edit_file_length");
    var efadein = document.getElementById("edit_fade_in");
    var efadeout = document.getElementById("edit_fade_out");
    var eloops = document.getElementById("edit_loops");
    var estartmin = document.getElementById("edit_start_pos_min");
    var estartsec = document.getElementById("edit_start_pos_sec");
    var estartms = document.getElementById("edit_start_pos_ms");
    var estopmin = document.getElementById("edit_stop_pos_min");
    var estopsec = document.getElementById("edit_stop_pos_sec");
    var estopms = document.getElementById("edit_stop_pos_ms");
    var preview = document.getElementById("edit_preview");
    var save = document.getElementById("edit_save");
    var cancel = document.getElementById("edit_cancel");
    var revert = document.getElementById("edit_revert");

    etarget.innerHTML = "<option value=\"0\">Next Cue</option>";
    for (i = 1; i <= cueListLength; i++) {
        if (i !== currentCue) {
            var option = document.createElement("option");
            option.value = i;
            option.text = i + " - " + getDesc(i);
            etarget.add(option);
        }
    }
    clearEditStartPos();
    clearEditStopPos();
    efile.value = "";

    if (ctype === "memo") {
        hide("edit_param_table2");
        hide("edit_param_table3");
        hide("edit_param_table4");
        //show("edit_param_table5");
        hide("edit_preview");
        etitle.innerHTML = "Edit Cue - Memo";
        
        
    } else if (ctype === "wait") {
        hide("edit_param_table2");
        hide("edit_param_table3");
        show("edit_param_table4");
        //hide("edit_param_table5");
        hide("edit_preview");
        
        var emin = document.getElementById("wait_min_in");
        var esec = document.getElementById("wait_sec_in");
        var ems = document.getElementById("wait_ms_in");
        var dur = getCueDurAsArray(cueNum);

        etitle.innerHTML = "Edit Cue - " + getDesc(cueNum);
        enotes.value = getNotes(cueNum);
        edesc.value = getDesc(cueNum);
        eaction.value = getAction(cueNum);
        etarget.value = getTargetId(cueNum);
        setEditWaitDur(dur);
        
        emin.oninput = function() { validateRange(this); };
        emin.onblur = function() { validateRange(this, true); };
        esec.oninput = function() { validateRange(this); };
        esec.onblur = function() { validateRange(this, true); };
        ems.oninput = function() { validateRange(this); };
        ems.onblur = function() { validateRange(this, true); };
        
        
    } else if (ctype === "audio" || ctype === "blank_audio") {
        show("edit_param_table2");
        show("edit_param_table3");
        hide("edit_param_table4");
        //hide("edit_param_table5");
        show("edit_preview");
        
        var estartmin = document.getElementById("edit_start_pos_min");
        var estartsec = document.getElementById("edit_start_pos_sec");
        var estartms = document.getElementById("edit_start_pos_ms");
        var estopmin = document.getElementById("edit_stop_pos_min");
        var estopsec = document.getElementById("edit_stop_pos_sec");
        var estopms = document.getElementById("edit_stop_pos_ms");

        etitle.innerHTML = "Edit Cue - " + getDesc(cueNum);
        enotes.value = getNotes(cueNum);
        edesc.value = getDesc(cueNum);
        efadein.value = getFadeIn(cueNum);
        efadeout.value = getFadeOut(cueNum);
        eloops.value = getLoops(cueNum);
        eaction.value = getAction(cueNum);
        etarget.value = getTargetId(cueNum);
        setEditVol(getVol(cueNum));
        setEditPan(getPan(cueNum));
        setEditPitch(getPitch(cueNum));
        setEditStartPos(getStartPosAsArray(cueNum));
        setEditStopPos(getStopPosAsArray(cueNum));

        if (allCueAudioFiles[cueNum]) {
            efilename.value = allCueAudioFiles[cueNum].name;
            setEditFileLength(getFileDurInSecs(cueNum));
            setEditStartPosMax(getFileDurAsArray(cueNum));
            setEditStopPosMax(getFileDurAsArray(cueNum));
        } else {
            efilename.value = "";
            efile.value = "";
            setEditFileLength(null);
            // Max pos already cleared above
        }

        evol.oninput = function() { setEditVol(evol.value); setLiveCueVol(cueNum, evol.value); };
        evol.ondblclick = function() { setEditVol(0); setLiveCueVol(cueNum, 0); };
        epan.oninput = function() { setEditPan(epan.value); setLiveCuePan(cueNum, epan.value); };
        epan.ondblclick = function() { setEditPan(0); setLiveCuePan(cueNum, 0); };
        epitch.oninput = function() { setEditPitch(epitch.value); setLiveCuePitch(cueNum, epitch.value); };
        epitch.ondblclick = function() { setEditPitch(100); setLiveCuePitch(cueNum, 100); };
        estartmin.oninput = function() { validateRange(this); };
        estartmin.onblur = function() { validateRange(this, true); };
        estartsec.oninput = function() { validateRange(this); };
        estartsec.onblur = function() { validateRange(this, true); };
        estartms.oninput = function() { validateRange(this); };
        estartms.onblur = function() { validateRange(this, true); };
        estopmin.oninput = function() { validateRange(this); };
        estopmin.onblur = function() { validateRange(this, true); };
        estopsec.oninput = function() { validateRange(this); };
        estopsec.onblur = function() { validateRange(this, true); };
        estopms.oninput = function() { validateRange(this); };
        estopms.onblur = function() { validateRange(this, true); };
        efile.onchange = function() {
            var filename = efile.files[0].name;
            var type = efile.files[0].type;
            console.log("file name: " + filename);
            console.log("file MIME: " + type);
            if (type.indexOf("audio") === -1) {
                alert(filename + " is not an audio file. Please try again.");
                efile.value = "";
                return;
            }
            efilename.value = filename;
            setEditButtonLock(true);
            syncDataFromSourceBuffer(cueNum, efile.files[0]); // Updates filelength, stop position, and start position maximum
            if (!edesc.value) {
                edesc.value = filename;
            }
        };

    } else if (ctype === "control") {
        hide("edit_preview");
        enotes.value = getNotes(cueNum);
        edesc.value = getDesc(cueNum);
        eaction.value = getAction(cueNum);
        etarget.value = getTargetId(cueNum);
        // When control stuff is finished...
    }

    showEditCueMenu();
}

function setEditVol(vol) {
    document.getElementById("edit_vol").innerHTML = vol + "dB";
    document.getElementById("vol_in").value = vol;
}

function setEditPan(pan) {
    document.getElementById("edit_pan").innerHTML = pan;
    document.getElementById("pan_in").value = pan;
}

function setEditPitch(pitch) {
    document.getElementById("edit_pitch").innerHTML = pitch + "%";
    document.getElementById("pitch_in").value = pitch;
}

function setLiveCueVol(cueNum, vol) {
    if (typeof currentlyPlaying !== "undefined" && currentlyPlaying[cueNum])
        currentlyPlaying[cueNum].gainNode.gain.value = dBToGain(vol);
    if (typeof previewing !== "undefined" && previewing)
        previewing.gainNode.gain.value = dBToGain(vol);
}

function setLiveCuePan(cueNum, pan) {
    if (typeof currentlyPlaying !== "undefined" && currentlyPlaying[cueNum])
        currentlyPlaying[cueNum].panNode.pan.value = pan / 50;
    if (typeof previewing !== "undefined" && previewing)
        previewing.panNode.pan.value = pan / 50;
}

function setLiveCuePitch(cueNum, pitch) {
    if (typeof currentlyPlaying == null)
        return;
    if (typeof previewing !== "undefined" && previewing)
        return;
    // TBD
}

function setEditFileLength(duration) {
    var efilelength = document.getElementById("edit_file_length");
    if (!duration)
        efilelength.innerHTML = "---";
    else
        efilelength.innerHTML = secToTime(duration);
}

function setEditStartPos(arr) {
    var emin = document.getElementById("edit_start_pos_min");
    var esec = document.getElementById("edit_start_pos_sec");
    var ems = document.getElementById("edit_start_pos_ms");
    emin.value = arr[0];
    esec.value = arr[1];
    ems.value = arr[2];
}

function getEditStartPos() {
    var emin = document.getElementById("edit_start_pos_min");
    var esec = document.getElementById("edit_start_pos_sec");
    var ems = document.getElementById("edit_start_pos_ms");
    return arrayToSec([emin.value, esec.value, ems.value]);
}

function setEditStartPosMax(arr) {
    var emin = document.getElementById("edit_start_pos_min");
    var esec = document.getElementById("edit_start_pos_sec");
    var ems = document.getElementById("edit_start_pos_ms");
    emin.max = arr[0];
    esec.max = arr[1];
    ems.max = arr[2];
}

function setEditStopPos(arr) {
    var emin = document.getElementById("edit_stop_pos_min");
    var esec = document.getElementById("edit_stop_pos_sec");
    var ems = document.getElementById("edit_stop_pos_ms");
    emin.value = arr[0];
    esec.value = arr[1];
    ems.value = arr[2];
}

function getEditStopPos() {
    var emin = document.getElementById("edit_stop_pos_min");
    var esec = document.getElementById("edit_stop_pos_sec");
    var ems = document.getElementById("edit_stop_pos_ms");
    return arrayToSec([parseInt(emin.value), parseInt(esec.value), parseInt(ems.value)]);
}

function setEditStopPosMax(arr) {
    var emin = document.getElementById("edit_stop_pos_min");
    var esec = document.getElementById("edit_stop_pos_sec");
    var ems = document.getElementById("edit_stop_pos_ms");
    emin.max = arr[0];
    esec.max = arr[1];
    ems.max = arr[2];
}

function clearEditStartPos() {
    var efilelength = document.getElementById("edit_file_length");
    setEditStartPos([0, 0, 0]);
    if (efilelength.innerHTML !== "---") {
        setEditStartPosMax(timeToArray(efilelength.innerHTML));
    } else {
        setEditStartPosMax([0, 0, 0]);
    }
}

function clearEditStopPos() {
    var efilelength = document.getElementById("edit_file_length");
    if (efilelength.innerHTML !== "---") {
        setEditStopPos(timeToArray(efilelength.innerHTML));
        setEditStopPosMax(timeToArray(efilelength.innerHTML));
    } else {
        setEditStopPos([0, 0, 0]);
        setEditStopPosMax([0, 0, 0]);
    }
}

function getEditWaitDur() {
    var emin = document.getElementById("wait_min_in");
    var esec = document.getElementById("wait_sec_in");
    var ems = document.getElementById("wait_ms_in");
    return arrayToSec([emin.value, esec.value, ems.value]);
}

function setEditWaitDur(arr) {
    var emin = document.getElementById("wait_min_in");
    var esec = document.getElementById("wait_sec_in");
    var ems = document.getElementById("wait_ms_in");
    emin.value = arr[0];
    esec.value = arr[1];
    ems.value = arr[2];
}

function clearEditWaitDur() {
    setEditWaitDur([0, 0, 0]);
}

function validateRange(self, fillBlank) {
    var min = parseInt(self.min);
    var max = parseInt(self.max);
    
    self.value = self.value.replace("-", "").replace("+", "");
    if (fillBlank && self.value == "") {
        self.value = 0;
        return;
    } else if (max != null && self.value > max) {
        self.value = max;
        return;
    } else if (min != null && self.value < min) {
        self.value = min;
    }
}

function saveEditedCue(cueNum) {
    cueNum = cueNum || currentCue;
    var ctype = getType(cueNum);

    var enotes = document.getElementById("edit_notes");
    var edesc = document.getElementById("edit_desc");
    var evol = document.getElementById("vol_in");
    var epan = document.getElementById("pan_in");
    var epitch = document.getElementById("pitch_in");
    var eaction = document.getElementById("edit_action");
    var etarget = document.getElementById("edit_target");
    var efile = document.getElementById("edit_file");
    var efilename = document.getElementById("edit_filename");
    var efilelength = document.getElementById("edit_file_length");
    var efadein = document.getElementById("edit_fade_in");
    var efadeout = document.getElementById("edit_fade_out");
    var eloops = document.getElementById("edit_loops");
    var estartmin = document.getElementById("edit_start_pos_min");
    var estartsec = document.getElementById("edit_start_pos_sec");
    var estartms = document.getElementById("edit_start_pos_ms");
    var estopmin = document.getElementById("edit_stop_pos_min");
    var estopsec = document.getElementById("edit_stop_pos_sec");
    var estopms = document.getElementById("edit_stop_pos_ms");
    var preview = document.getElementById("edit_preview");
    var save = document.getElementById("edit_save");
    var cancel = document.getElementById("edit_cancel");
    var revert = document.getElementById("edit_revert");


    if (ctype === "memo") {
        setNotes(cueNum, enotes.value);
        setDesc(cueNum, edesc.value);
        
        
    } else if (ctype === "wait") {
        setCueDur(cueNum, getEditWaitDur());
        setNotes(cueNum, enotes.value);
        setDesc(cueNum, edesc.value);
        
        
    } else if (ctype === "audio" || ctype === "blank_audio") {
        setNotes(cueNum, enotes.value);
        setDesc(cueNum, edesc.value);
        setVol(cueNum, evol.value);
        setPan(cueNum, epan.value);
        setPitch(cueNum, epitch.value);
        setAudioFile(cueNum, efile.files[0]);
        //syncDataFromSourceBuffer(cueNum); // Set editMust be after audio file is in place and before setStopPos
        setStartPos(cueNum, arrayToSec([estartmin.value, estartsec.value, estartms.value]));
        setStopPos(cueNum, arrayToSec([estopmin.value, estopsec.value, estopms.value])); // In case user changed it from file length
        console.log("updated duration: " + secToTime(getStopPosInSecs(cueNum) - getStartPosInSecs(cueNum)));
        setCueDur(cueNum, getStopPosInSecs(cueNum) - getStartPosInSecs(cueNum)); // Duration of cue based on start and stop position
        setFileDur(cueNum, timeToSec(efilelength.innerHTML)); // Full duration of file
        setFadeIn(cueNum, efadein.value);
        setFadeOut(cueNum, efadeout.value);
        setLoops(cueNum, eloops.value);
        setAction(cueNum, eaction.value);
        setTarget(cueNum, etarget.value);
    }
    
    // Release event handlers
    evol.oninput = function() {};
    evol.ondblclick = function() {};
    epan.oninput = function() {};
    epan.ondblclick = function() {};
    epitch.oninput = function() {};
    epitch.ondblclick = function() {};
    efile.onchange = function() {};

    hideEditCueMenu();
}

function cancelEditedCue(cueNum) {
    cueNum = cueNum || currentCue;
    var ctype = getType(cueNum);
    
    if (ctype === "audio" || ctype === "blank_audio") {
        // Reset live controls to previous values
        setLiveCueVol(cueNum, getVol(cueNum));
        setLiveCuePan(cueNum, getPan(cueNum));
        setLiveCuePitch(cueNum, getPitch(cueNum));
    }
    
    hideEditCueMenu();
}

function showNewCueMenu() {
    hideEditCueMenu();
    document.getElementById("cue_type").selectedIndex = 0;
    show("new_cue_menu");
    keyListeners(false, false, true, false);
}

function hideNewCueMenu() {
    hide("new_cue_menu");
    keyListeners(false, false, false, false);
}

function showEditCueMenu() {
    hideNewCueMenu();
    show("edit_cue_menu");
    keyListeners(false, false, false, true);
}

function hideEditCueMenu() {
    if (previewing) {
        previewing.stop(0);
    }
    hide("edit_cue_menu");
    keyListeners(false, false, false, false);
}

function select(cueNum) {
    var cues = document.getElementsByClassName("selected");

    // Check if there is a selected cue and if we're already on the desired one
    if (cues[0] && cues[0].id == cueNum) {
        return 0;
    } else if (cues[0]) {
        cues[0].className = cues[0].className.replace("selected", "deselected");
        closeAll();
    }

    var selCue = document.getElementById(cueNum);
    selCue.className = selCue.className.replace("deselected", "selected");
    currentCue = cueNum; // Global tracker
    checkButtonLock();
}

function checkButtonLock() {
    setButtonLock(typeof currentlyPlaying !== "undefined" && currentlyPlaying[currentCue])
}

function setButtonLock(val) {
    var go = document.getElementById("go_button");
    var editfile = document.getElementById("edit_file");
    var editremove = document.getElementById("edit_remove");
    var editfieldset = document.getElementById("edit_fade_pos_loop_fieldset");
    
    go.disabled = val;
    editfile.disabled = val;
    editremove.disabled = val;
    editfieldset.disabled = val;
}

function setEditButtonLock(val) {
    var preview = document.getElementById("edit_preview");
    var save = document.getElementById("edit_save");
    var cancel = document.getElementById("edit_cancel");
    var revert = document.getElementById("edit_revert");
    
    preview.disabled = val;
    save.disabled = val;
    cancel.disabled = val;
    revert.disabled = val;
    
}

function updateProgressBar(cueNum, percent, remaining) {
    var bar = document.getElementById(cueNum + "0005");
    if (remaining <= 5) {
        bar.style.backgroundImage = "url(red-dot.jpg)";
        bar.style.backgroundSize = percent + "% 100%";
    } else {
        bar.style.backgroundSize = percent + "% 100%";
    }
}

function resetProgressBar(cueNum) {
    var bar = document.getElementById(cueNum + "0005");
    bar.style.backgroundImage = "url(green-dot.jpg)";
    bar.style.backgroundSize = 0;
}

function secToTime(sec) {
    if (sec === "")
        sec = 0;
    sec = parseFloat(sec);
    var ms = (sec % 1).toFixed(1);
    var min = parseInt(sec / 60);
    var sec = (sec % 60).toFixed(2);
    return min + ":" + pad(sec) + String(ms).substring(1);
}

function timeToSec(time) {
    if (time === "")
        return 0;
    var min = parseInt(time.substring(0, time.indexOf(":")));
    var sec = parseFloat(time.substring(time.indexOf(":") + 1));
    return (60 * min + sec).toFixed(2);
}

function secToArray(sec) {
    var arr = [0, 0, 0];
    if (sec === 0 || sec === "")
        return arr;
    sec = parseFloat(sec);
    arr[0] = parseInt(sec / 60);
    arr[1] = parseInt(sec % 60);
    arr[2] = parseInt((sec % 1).toFixed(3) * 1000);
    return arr;   
}

function timeToArray(time) {
    return secToArray(timeToSec(time));
}

function arrayToSec(arr) {
    return parseInt(arr[0]) * 60 + parseInt(arr[1]) + parseInt(arr[2]) / 1000;
}

function arrayToTime(arr) {
    return secToTime(arrayToSec(arr));
}

function pad(n) {
    n = parseInt(n);
    if (n >=0 && n < 10) 
        return "0" + n; 
    if(n > -10 && n < 0) 
        return "-0" + Math.abs(n); 
    return n; 
}

function getType(cueNum) {
    return document.getElementById(cueNum + "00010001").innerHTML;
}

function setType(cueNum, type) {
    document.getElementById(cueNum + "00010001").innerHTML = type;
}

function getEnabled(cueNum) {
    return document.getElementById(cueNum + "00010002").checked;
}

function setEnabled(cueNum, state) {
    document.getElementById(cueNum + "00010002").checked = state;
}

function getNotes(cueNum) {
    return document.getElementById(cueNum + "00040002").innerHTML;
}

function setNotes(cueNum, notes) {
    document.getElementById(cueNum + "00040001").value = notes;
    document.getElementById(cueNum + "00040002").innerHTML = notes;
}

function getDesc(cueNum) {
    return document.getElementById(cueNum + "00050002").innerHTML;
}

function setDesc(cueNum, desc) {
    document.getElementById(cueNum + "00050001").value = desc;
    document.getElementById(cueNum + "00050002").innerHTML = desc;
}

function setCueDur(cueNum, sec) {
    var dur = document.getElementById(cueNum + "00060001");
    if (sec == null)
        dur.innerHTML = "";
    else
        dur.innerHTML = secToTime(sec);
}

function getCueDur(cueNum) {
    return document.getElementById(cueNum + "00060001").innerHTML;
}

function getCueDurInSecs(cueNum) {
    return parseFloat(timeToSec(getCueDur(cueNum)));
}

function getCueDurAsArray(cueNum) {
    return timeToArray(getCueDur(cueNum));
}

function calculateCueDurInSecs(cueNum, fullDur) {
    fullDur = fullDur || getFileDurInSecs(cueNum);
    return fullDur - getStartPosInSecs(cueNum) - (fullDur - getStopPosInSecs(cueNum));
}

function setFileDur(cueNum, sec) {
    var dur = document.getElementById(cueNum + "00060006");
    if (!sec)
        dur.innerHTML = "";
    else
        dur.innerHTML = secToTime(sec);
}

function getFileDur(cueNum) {
    return document.getElementById(cueNum + "00060006").innerHTML;
}

function getFileDurInSecs(cueNum) {
    return parseFloat(timeToSec(getFileDur(cueNum)));
}

function getFileDurAsArray(cueNum) {
    return timeToArray(getFileDur(cueNum));
}

function getStartPos(cueNum) {
    return secToTime(getStartPosInSecs(cueNum));
}

function getStartPosInSecs(cueNum) {
    return parseFloat(document.getElementById(cueNum + "00060002").innerHTML);
}

function getStartPosAsArray(cueNum) {
    return secToArray(getStartPosInSecs(cueNum));
}

function setStartPos(cueNum, pos) {
    document.getElementById(cueNum + "00060002").innerHTML = pos;
}

function getStopPos(cueNum) {
    return secToTime(getStopPosInSecs(cueNum));
}

function getStopPosInSecs(cueNum) {
    return parseFloat(document.getElementById(cueNum + "00060003").innerHTML);
}

function getStopPosAsArray(cueNum) {
    return secToArray(getStopPosInSecs(cueNum));
}

function setStopPos(cueNum, pos) {
    document.getElementById(cueNum + "00060003").innerHTML = pos;
}

function getFadeIn(cueNum) {
    return parseFloat(document.getElementById(cueNum + "00060004").innerHTML);
}

function setFadeIn(cueNum, length) {
    document.getElementById(cueNum + "00060004").innerHTML = length;
}

function getFadeOut(cueNum) {
    return parseFloat(document.getElementById(cueNum + "00060005").innerHTML);
}

function setFadeOut(cueNum, length) {
    document.getElementById(cueNum + "00060005").innerHTML = length;
}

function setElapsed(cueNum, sec) {
    var elapsed = document.getElementById(cueNum + "00070001");
    if (!sec && sec !== 0)
        elapsed.innerHTML = "";
    else
        elapsed.innerHTML = secToTime(sec);
}

function setRemaining(cueNum, sec) {
    var remaining = document.getElementById(cueNum + "00080001");
    if (!sec && sec !== 0)
        remaining.innerHTML = "";
    else
        remaining.innerHTML = secToTime(sec);
}

function getVol(cueNum) {
    return parseInt(document.getElementById(cueNum + "00090001").innerHTML);
}

function setVol(cueNum, vol) {
    document.getElementById(cueNum + "00090001").innerHTML = vol + "dB";
}

function getPan(cueNum) {
    return parseInt(document.getElementById(cueNum + "000100001").innerHTML);
}

function setPan(cueNum, pan) {
    document.getElementById(cueNum + "000100001").innerHTML = pan;
}

function getPitch(cueNum) {
    return parseInt(document.getElementById(cueNum + "000110001").innerHTML);
}

function setPitch(cueNum, pitch) {
    document.getElementById(cueNum + "000110001").innerHTML = pitch + "%";
}

function getLoops(cueNum) {
    return parseInt(document.getElementById(cueNum + "000120001").innerHTML);
}

function setLoops(cueNum, loops) {
    document.getElementById(cueNum + "000120001").innerHTML = loops;
}

function getAction(cueNum) {
    return document.getElementById(cueNum + "000130001").innerHTML;
}

function setAction(cueNum, action) {
    document.getElementById(cueNum + "000130001").innerHTML = action;
}

function getTarget(cueNum) {
    return document.getElementById(cueNum + "000140001").innerHTML;
}

function getTargetId(cueNum) {
    return parseInt(document.getElementById(cueNum + "000140002").innerHTML);
}

function setTarget(cueNum, targetId) {
    targetId = parseInt(targetId);
    if (targetId === 0) {
        document.getElementById(cueNum + "000140001").innerHTML = "Next Cue";
        document.getElementById(cueNum + "000140002").innerHTML = 0;
    } else {
        document.getElementById(cueNum + "000140001").innerHTML = targetId + " - " + getDesc(targetId);
        document.getElementById(cueNum + "000140002").innerHTML = targetId;
    }
}

function getAudioFile(cueNum) {
    return allCueAudioFiles[cueNum];
}

function setAudioFile(cueNum, file, overwriteNull) {
    if (file || overwriteNull) {
        allCueAudioFiles[cueNum] = file;
        console.dir(allCueAudioFiles);
    }
}

function hasAudioFile(cueNum) {
    return allCueAudioFiles[cueNum] !== null;
}

function deleteAudioFile(cueNum) {
    cueNum = cueNum || currentCue;
    if (allCueAudioFiles[cueNum]) {
        delete allCueAudioFiles[cueNum];
    }
    var efile = document.getElementById("edit_file");
    var efilename = document.getElementById("edit_filename");
    efile.value = "";
    efilename.value = "";
    setEditFileLength(null);
    setCueDur(cueNum, null);
    setFileDur(cueNum, null);
    setStartPos(cueNum, null);
    setStartPosMax(cueNum, null);
    setStopPos(cueNum, null);
    clearEditStartPos();
    setEditStartPosMax([0, 0, 0]);
    setEditStopPosMax([0, 0, 0]);
    clearEditStopPos();
}

function initializeCue(cueNum) {
    var ctype = getType(cueNum);

    setEnabled(cueNum, true);
    setNotes(cueNum, "");
    
    if (ctype === "wait") {
        setDesc(cueNum, "Wait");
        setCueDur(cueNum, 0);
        setFileDur(cueNum, 0);
        setAction(cueNum, "SP");
        setTarget(cueNum, 0);
        
    } else {
        setDesc(cueNum, "");

        if (ctype !== "memo") {
            setCueDur(cueNum, null);
            setFileDur(cueNum, null);
            setStartPos(cueNum, 0);
            setStopPos(cueNum, 0);
            setFadeIn(cueNum, 0);
            setFadeOut(cueNum, 0);
            setVol(cueNum, 0);
            setPan(cueNum, 0);
            setPitch(cueNum, 100);
            setLoops(cueNum, 1);
            setAction(cueNum, "SA");
            setTarget(cueNum, 0);
        }
    }

}

function download() {
    document.location = "data:text/attachment;base64," + utf8_to_b64("<!DOCTYPE html><html>" + document.documentElement.innerHTML + "</html>");
}

function utf8_to_b64(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
}

function createCue(cueType) {
    // Just for safety
    if (cueListLength * 100010000 >= Number.MAX_SAFE_INTEGER) {
        console.log("Cannot add cue. Cue list has reached max length.");
        alert("Cannot add cue. Cue list has reached max length.");
        return 0;
    }

    var cueList = document.getElementById("cue_list");
    cueType = cueType || document.getElementById("cue_type").value;
    var cueData = [];

    var cue = cueList.insertRow(cueListLength + 1); // Create cue in last spot
    var cueId = String(cueListLength + 1);
    cue.id = cueId;
    cue.className = "cue-row deselected";
    cue.setAttribute('onclick', 'select(' + cue.id + ')');
    cueId += "000"; // Seperator for preventing duplicate ids

    if (cueType === "audio" || cueType === "blank_audio") {
        for (var i = 1; i <= 14; i++) {
            var cell = cue.insertCell(i - 1);
            cell.id = cueId + i;
            var cellId = cell.id + "000";
            switch (i) {
                case 1:
                    cell.className = "cue audio en";

                    var ctype = document.createElement("div");
                    ctype.className = "hidden";
                    ctype.id = cellId + "1";
                    ctype.innerHTML = cueType;
                    cell.appendChild(ctype);

                    var input = document.createElement("input");
                    input.type = "checkbox";
                    input.id = cellId + "2";
                    input.name = "en";
                    input.checked = true;
                    cell.appendChild(input);
                    break;
                case 2:
                    cell.className = "cue audio q";
                    cell.innerHTML = cueId.substring(0, cueId.length - 3);
                    break;
                case 3:
                    cell.className = "cue audio hkey";
                    break;
                case 4:
                    cell.className = "cue audio notes";

                    var input = document.createElement("input");
                    input.type = "text";
                    input.id = cellId + "1";
                    input.className = "hidden long";

                    var display = document.createElement("div");
                    display.id = cellId + "2";
                    display.className = "display long visible";

                    cell.appendChild(input);
                    cell.appendChild(display);

                    cell.setAttribute('onclick', 'edit(' + input.id + ')');
                    break;
                case 5:
                    cell.className = "cue audio desc progress";
                    
                    var input = document.createElement("input");
                    input.type = "text";
                    input.id = cellId + "1";
                    input.className = "long hidden";

                    var display = document.createElement("div");
                    display.id = cellId + "2";
                    display.className = "display long visible";
                    
                    cell.appendChild(input);
                    cell.appendChild(display);

                    cell.setAttribute('onclick', 'edit(' + input.id + ')');
                    break;
                case 6:
                    cell.className = "cue audio dur";

                    var displaydur = document.createElement("span");
                    displaydur.id = cellId + "1";
                    displaydur.className = "visible";
                    
                    var startPos = document.createElement("div");
                    startPos.id = cellId + "2";
                    startPos.className = "hidden";
                    
                    var stopPos = document.createElement("div");
                    stopPos.id = cellId + "3";
                    stopPos.className = "hidden";
                    
                    var fadeIn = document.createElement("div");
                    fadeIn.id = cellId + "4";
                    fadeIn.className = "hidden";
                    
                    var fadeOut = document.createElement("div");
                    fadeOut.id = cellId + "5";
                    fadeOut.className = "hidden";
                    
                    var fullfileduration = document.createElement("div");
                    fullfileduration.id = cellId + "6";
                    fullfileduration.className = "hidden";
                    
                    cell.appendChild(displaydur);
                    cell.appendChild(startPos);
                    cell.appendChild(stopPos);
                    cell.appendChild(fadeIn);
                    cell.appendChild(fadeOut);
                    cell.appendChild(fullfileduration);
                    break;
                case 7:
                    cell.className = "cue audio elapsed";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";
                    cell.appendChild(display);
                    break;
                case 8:
                    cell.className = "cue audio remaining";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";
                    cell.appendChild(display);
                    break;
                case 9:
                    cell.className = "cue audio vol";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";
                    cell.appendChild(display);
                    break;
                case 10:
                    cell.className = "cue audio pan";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";
                    cell.appendChild(display);
                    break;
                case 11:
                    cell.className = "cue audio pitch";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";
                    cell.appendChild(display);
                    break;
                case 12:
                    cell.className = "cue audio loops";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";
                    cell.appendChild(display);
                    break;
                case 13:
                    cell.className = "cue audio action";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";
                    cell.appendChild(display);
                    break;
                case 14:
                    cell.className = "cue audio target";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";

                    var targetId = document.createElement("span");
                    targetId.id = cellId + "2";
                    targetId.className = "hidden";

                    cell.appendChild(display);
                    cell.appendChild(targetId);
                    break;
                default:
                    break;
            }
            cueData.push(cell);
        }
    } else if (cueType === "memo") {
        for (var i = 1; i <= 5; i++) {
            var cell = cue.insertCell(i - 1);
            cell.id = cueId + i;
            var cellId = cell.id + "000";
            switch (i) {
                case 1:
                    cell.className = "cue memo en";

                    var ctype = document.createElement("div");
                    ctype.className = "hidden";
                    ctype.id = cellId + "1";
                    ctype.innerHTML = cueType;
                    cell.appendChild(ctype);

                    var checkbox = document.createElement("input");
                    checkbox.type = "checkbox";
                    checkbox.id = cellId + "2";
                    checkbox.className = "hidden";
                    checkbox.name = "en";
                    checkbox.checked = false;

                    cell.appendChild(ctype);
                    cell.appendChild(checkbox);
                    break;
                case 2:
                    cell.className = "cue memo q";
                    cell.innerHTML = cueId.substring(0, cueId.length - 3);
                    break;
                case 3:
                    cell.className = "cue memo hkey";
                    break;
                case 4:
                    cell.className = "cue memo notes";
                    cell.colSpan = 4;

                    var input = document.createElement("input");
                    input.type = "text";
                    input.id = cellId + "1";
                    input.className = "long hidden";

                    var display = document.createElement("div");
                    display.id = cellId + "2";
                    display.className = "display long visible";

                    cell.appendChild(input);
                    cell.appendChild(display);
                    cell.setAttribute('onclick', 'edit(' + input.id + ')');
                    break;
                case 5:
                    cell.className = "cue memo desc";
                    cell.colSpan = 7;

                    var input = document.createElement("input");
                    input.type = "text";
                    input.id = cellId + "1";
                    input.className = "long hidden";

                    var display = document.createElement("div");
                    display.id = cellId + "2";
                    display.className = "display long visible";

                    cell.appendChild(input);
                    cell.appendChild(display);
                    cell.setAttribute('onclick', 'edit(' + input.id + ')');
                    break;
                default:
                    break;
            }
        }
    } else if (cueType === "wait") {
        for (var i = 1; i <= 14; i++) {
            var cell = cue.insertCell(i - 1);
            cell.id = cueId + i;
            var cellId = cell.id + "000";
            switch (i) {
                case 1:
                    cell.className = "cue wait en";

                    var ctype = document.createElement("div");
                    ctype.className = "hidden";
                    ctype.id = cellId + "1";
                    ctype.innerHTML = cueType;
                    cell.appendChild(ctype);

                    var input = document.createElement("input");
                    input.type = "checkbox";
                    input.id = cellId + "2";
                    input.name = "en";
                    input.checked = true;
                    cell.appendChild(input);
                    break;
                case 2:
                    cell.className = "cue wait q";
                    cell.innerHTML = cueId.substring(0, cueId.length - 3);
                    break;
                case 3:
                    cell.className = "cue wait hkey";
                    break;
                case 4:
                    cell.className = "cue wait notes";

                    var input = document.createElement("input");
                    input.type = "text";
                    input.id = cellId + "1";
                    input.className = "hidden long";

                    var display = document.createElement("div");
                    display.id = cellId + "2";
                    display.className = "display long visible";

                    cell.appendChild(input);
                    cell.appendChild(display);

                    cell.setAttribute('onclick', 'edit(' + input.id + ')');
                    break;
                case 5:
                    cell.className = "cue wait desc progress";
                    
                    var input = document.createElement("input");
                    input.type = "text";
                    input.id = cellId + "1";
                    input.className = "long hidden";

                    var display = document.createElement("div");
                    display.id = cellId + "2";
                    display.className = "display long visible";
                    
                    cell.appendChild(input);
                    cell.appendChild(display);

                    cell.setAttribute('onclick', 'edit(' + input.id + ')');
                    break;
                case 6:
                    cell.className = "cue wait dur";

                    var displaydur = document.createElement("span");
                    displaydur.id = cellId + "1";
                    displaydur.className = "visible";
                    
                    var startPos = document.createElement("div");
                    startPos.id = cellId + "2";
                    startPos.className = "hidden";
                    
                    var stopPos = document.createElement("div");
                    stopPos.id = cellId + "3";
                    stopPos.className = "hidden";
                    
                    var fadeIn = document.createElement("div");
                    fadeIn.id = cellId + "4";
                    fadeIn.className = "hidden";
                    
                    var fadeOut = document.createElement("div");
                    fadeOut.id = cellId + "5";
                    fadeOut.className = "hidden";
                    
                    var fullfileduration = document.createElement("div");
                    fullfileduration.id = cellId + "6";
                    fullfileduration.className = "hidden";
                    
                    cell.appendChild(displaydur);
                    cell.appendChild(startPos);
                    cell.appendChild(stopPos);
                    cell.appendChild(fadeIn);
                    cell.appendChild(fadeOut);
                    cell.appendChild(fullfileduration);
                    break;
                case 7:
                    cell.className = "cue wait elapsed";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";
                    cell.appendChild(display);
                    break;
                case 8:
                    cell.className = "cue wait remaining";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";
                    cell.appendChild(display);
                    break;
                case 9:
                    cell.className = "cue wait vol";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";
                    cell.appendChild(display);
                    break;
                case 10:
                    cell.className = "cue wait pan";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";
                    cell.appendChild(display);
                    break;
                case 11:
                    cell.className = "cue wait pitch";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";
                    cell.appendChild(display);
                    break;
                case 12:
                    cell.className = "cue wait loops";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";
                    cell.appendChild(display);
                    break;
                case 13:
                    cell.className = "cue wait action";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";
                    cell.appendChild(display);
                    break;
                case 14:
                    cell.className = "cue wait target";

                    var display = document.createElement("span");
                    display.id = cellId + "1";
                    display.className = "visible";

                    var targetId = document.createElement("span");
                    targetId.id = cellId + "2";
                    targetId.className = "hidden";

                    cell.appendChild(display);
                    cell.appendChild(targetId);
                    break;
                default:
                    break;
            }

        }
    }
    cueListLength++;
    moveCue(cueListLength, currentCue + 1); // Move to insertion point
    initializeCue(currentCue + 1);
    return cue;
}

function updateCueNumber(currNum, shift) {
    if (currNum + shift <= 0) {
        console.log("Cannot update cue. Cue number " + (currNum + shift) + " is invalid. Cue number cannot be less than 1.");
        return;
    }

    var cue = document.getElementById(currNum);
    var cueType = getType(currNum);
    var newCueId = String(currNum + shift) + "000";

    if (cueType === "audio" || cueType === "blank_audio" || cueType === "wait") {
        for (var i = 1; i <= cue.cells.length; i++) {
            var cell = cue.cells[i - 1];
            var oldCellId = cell.id + "000";
            cell.id = newCueId + i;
            var newCellId = cell.id + "000";
            switch (i) {
                case 1:
                    var ctype = document.getElementById(oldCellId + "1");
                    var engaged = document.getElementById(oldCellId + "2");
                    ctype.id = newCellId + "1";
                    engaged.id = newCellId + "2";
                    break;
                case 2:
                    cell.innerHTML = newCueId.substring(0, newCueId.length - 3);
                    break;
                case 3:
                    break;
                case 4:
                    var input = document.getElementById(oldCellId + "1");
                    var display = document.getElementById(oldCellId + "2");
                    input.id = newCellId + "1";
                    display.id = newCellId + "2";
                    cell.setAttribute('onclick', 'edit(' + input.id + ')');
                    break;
                case 5:
                    var input = document.getElementById(oldCellId + "1");
                    var display = document.getElementById(oldCellId + "2");
                    input.id = newCellId + "1";
                    display.id = newCellId + "2";

                    cell.setAttribute('onclick', 'edit(' + input.id + ')');
                    break;
                case 6:
                    var displaydur = document.getElementById(oldCellId + "1");
                    displaydur.id = newCellId + "1";
                    var startPos = document.getElementById(oldCellId + "2");
                    startPos.id = newCellId + "2";
                    var stopPos = document.getElementById(oldCellId + "3");
                    stopPos.id = newCellId + "3";
                    var fadeIn = document.getElementById(oldCellId + "4");
                    fadeIn.id = newCellId + "4";
                    var fadeOut = document.getElementById(oldCellId + "5");
                    fadeOut.id = newCellId + "5";
                    var fullfilelength = document.getElementById(oldCellId + "6");
                    fullfilelength.id = newCellId + "6";
                    break;
                case 7:
                    var display = document.getElementById(oldCellId + "1");
                    display.id = newCellId + "1";
                    break;
                case 8:
                    var display = document.getElementById(oldCellId + "1");
                    display.id = newCellId + "1";
                    break;
                case 9:
                    var display = document.getElementById(oldCellId + "1");
                    display.id = newCellId + "1";
                    break;
                case 10:
                    var display = document.getElementById(oldCellId + "1");
                    display.id = newCellId + "1";
                    break;
                case 11:
                    var display = document.getElementById(oldCellId + "1");
                    display.id = newCellId + "1";
                    break;
                case 12:
                    var display = document.getElementById(oldCellId + "1");
                    display.id = newCellId + "1";
                    break;
                case 13:
                    var display = document.getElementById(oldCellId + "1");
                    display.id = newCellId + "1";
                    break;
                case 14:
                    var display = document.getElementById(oldCellId + "1");
                    display.id = newCellId + "1";
                    var targetId = document.getElementById(oldCellId + "2");
                    targetId.id = newCellId + "2";
                    break;
                default:
                    break;
            }
        }
    } else if (cueType === "memo") {
        for (var i = 1; i <= cue.cells.length; i++) {
            var cell = cue.cells[i - 1];
            var oldCellId = cell.id + "000";
            cell.id = newCueId + i;
            var newCellId = cell.id + "000";
            switch (i) {
                case 1:
                    var ctype = document.getElementById(oldCellId + "1");
                    ctype.id = newCellId + "1";

                    var checkbox = document.getElementById(oldCellId + "2");
                    checkbox.id = newCellId + "2";
                    break;
                case 2:
                    cell.innerHTML = newCueId.substring(0, newCueId.length - 3);
                    break;
                case 3:
                    break;
                case 4:
                    var input = document.getElementById(oldCellId + "1");
                    input.id = newCellId + "1";
                    var display = document.getElementById(oldCellId + "2");
                    display.id = newCellId + "2";

                    cell.setAttribute('onclick', 'edit(' + input.id + ')');
                    break;
                case 5:
                    var input = document.getElementById(oldCellId + "1");
                    input.id = newCellId + "1";
                    var display = document.getElementById(oldCellId + "2");
                    display.id = newCellId + "2";

                    cell.setAttribute('onclick', 'edit(' + input.id + ')');
                    break;
                default:
                    break;
            }
        }
    }
    return cue;
}

function moveCue(currNum, insertPoint) {
    if (insertPoint > cueListLength) {
        alert("Cannot move cue. Cue number " + insertPoint + " is greater than cue list length.");
        return;
    } else if (insertPoint < 0) {
        alert("Cannot move cue. Cue number " + insertPoint + " is invalid. Cue Number cannot be less than 1.");
        return;
    }

    var cueList = document.getElementById("cue_list");
    var cue = document.getElementById(currNum);
    var tempCueEn = getEnabled(currNum);
    var tempCueNotes = getNotes(currNum);
    var tempCueDesc = getDesc(currNum);
    var tempCueFile = getAudioFile(currNum);
    var tempCue = updateCueNumber(currNum, insertPoint - currNum).innerHTML; // Update inner cue info to match final position and store in temp

    if (currNum < insertPoint) {
        for (var i = currNum + 1; i <= insertPoint; i++) {
            var tempEn = getEnabled(i);
            var tempNotes = getNotes(i);
            var tempDesc = getDesc(i);
            cueList.rows[i - 1].innerHTML = updateCueNumber(i, -1).innerHTML;
            cueList.rows[i].innerHTML = "";
            setAudioFile(i - 1, getAudioFile(i), true);
            setEnabled(i - 1, tempEn);
            setNotes(i - 1, tempNotes);
            setDesc(i - 1, tempDesc);
        }
    } else if (currNum > insertPoint) {
        for (var i = currNum - 1; i >= insertPoint; i--) {
            var tempEn = getEnabled(i);
            var tempNotes = getNotes(i);
            var tempDesc = getDesc(i);
            cueList.rows[i + 1].innerHTML = updateCueNumber(i, 1).innerHTML;
            cueList.rows[i].innerHTML = ""; // Overwrite to avoid duplicate id's
            setAudioFile(i + 1, getAudioFile(i), true);
            setEnabled(i + 1, tempEn);
            setNotes(i + 1, tempNotes);
            setDesc(i + 1, tempDesc);
        }
    }

    // Move cue to final position
    cueList.rows[insertPoint].innerHTML = tempCue;
    setEnabled(insertPoint, tempCueEn);
    setNotes(insertPoint, tempCueNotes);
    setDesc(insertPoint, tempCueDesc);
    setAudioFile(insertPoint, tempCueFile, true);
}