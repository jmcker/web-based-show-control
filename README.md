# Web Based Show Control #

# -- CURRENTLY UNDER DEVELOPMENT -- #

Web Based Show Control is inspired by Multiplay, a Windows-based theater cue playback system, and is designed to run in any browser that supports Web Audio API. It currently is capable of loading and playing back multiple audio files with control over volume, pan, fade in, fade out, start position, stop position, looping, and auto advancing. Wait cues have also been implemented in the current version.

# In development: #
* Saving shows
* Infinitely looping audio
* Pitch shifting
* Control cues
* End/fade advance and play actions
* Deleting and moving cues

# Future development: #
* MIDI cues using Web MIDI API
* Video cues
* OSC commands
* Display and style preference options
* Playback of hosted audio files (currently only local)

# Current issues #
* Memory usage has been a real problem -- Web Audio API buffers take up a lot of memory, especially for long audio files. The garbage collector seems to hold onto the buffers for too long after they've been used. Thus, playing back cues repeatedly or trying to load a large file quickly runs the browser out of memory.
* There is currently no way to save shows. All of the cue parameters are stored directly in the HTML source, so saving the page and reopening it locally preserves this data. I need to work on implementing FileSystem to preserve audio files before a show can actually be recalled.